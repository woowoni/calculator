const cal = (() => {

  const calculate = (firstNumber, key, secondNumber) => {
    firstNumber = parseFloat(firstNumber);
    secondNumber = parseFloat(secondNumber);

    if (key === 'plus') return firstNumber + secondNumber;
    if (key === 'minus') return firstNumber - secondNumber;
    if (key === 'times') return firstNumber * secondNumber;
    if (key === 'divide') return firstNumber / secondNumber;
  }

  const init = () => {
    const calculator = document.querySelector('[data-element="calculator"]');
    const keys = calculator.querySelector('[data-element="keys"]');
    const display = calculator.querySelector('[data-element="pad"]');

    keys.addEventListener('click', (e) => {
      if (!e.target.closest('button')) return;

      const key = e.target;
      const keyValue = key.textContent;
      const displayValue = display.textContent;
      const { type } = key.dataset;
      const { previousKeyType } = calculator.dataset;
      
      if (type === 'allClear') {
        display.textContent = '0';
      }

      if (type === 'number') {
        if (
          displayValue === '0' ||
          previousKeyType === 'operator'
        ) {
          display.textContent = keyValue;
        } else {
          display.textContent = displayValue + keyValue;
        }
      }

      if (type === 'operator') {
        const operatorKeys = keys.querySelectorAll('[data-type="operator"]')
        operatorKeys.forEach(el => {
          el.dataset.state = ''
        })
        key.dataset.state = 'selected';

        calculator.dataset.firstNumber = displayValue;
        calculator.dataset.operator = key.dataset.key;
      }

      if (type === 'decimal') {
        if (displayValue.indexOf('.') === -1) {
          display.textContent = displayValue + keyValue;
        } else {
          return;
        }  
      }

      if (type === 'equal') {
        const firstNumber = calculator.dataset.firstNumber;
        const operator = calculator.dataset.operator;
        const secondNumber = displayValue;
        const result = calculate(firstNumber, operator, secondNumber);
        display.textContent = parseFloat(result.toFixed(7));
      }
    
      calculator.dataset.previousKeyType = type;
    });
  }
  // Public Method & Valiable
  return {
      init: init,
  };
})();

$(document).ready(() => {
    cal.init();
});